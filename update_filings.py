import json
import random

import pysec

data = json.load(open('entities.json'))
company_filings = json.load(open('company_filings.json'))

MAX_SCRAPES_PER_RUN = 250

updated_vendors = {}

# Initial pass looking for new entities.
num_scrapes_left = MAX_SCRAPES_PER_RUN
for vendor in data:
    if num_scrapes_left <= 0:
        break
    if vendor in company_filings:
        continue
    profile = data[vendor]
    if 'cik' in profile:
        print('Processing new company ({}) w/ {} scrapes left.'.format(
            vendor, num_scrapes_left))
        cik = profile['cik']
        company_filings[vendor], num_scrapes = pysec.company_info(cik)
        num_scrapes_left -= num_scrapes
        updated_vendors[vendor] = True

dataKeys = list(data.keys())
for vendor in random.sample(dataKeys, len(dataKeys)):
    if num_scrapes_left <= 0:
        break
    if vendor in updated_vendors:
        continue
    profile = data[vendor]
    if 'cik' in profile:
        print('Updating {} w/ {} scrapes left.'.format(
            vendor, num_scrapes_left))
        cik = profile['cik']
        if vendor in company_filings:
            company_filings[vendor], num_scrapes = pysec.company_info(
                cik, old_info=company_filings[vendor])
        else:
            company_filings[vendor], num_scrapes = pysec.company_info(cik)
        num_scrapes_left -= num_scrapes

with open('company_filings.json', 'w') as outfile:
    json.dump(company_filings, outfile, indent=1)

