'''
Copyright (c) 2020 Jack Poulson <jack@techinquiry.org>

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

PySEC is a simple scraper for retrieving SEC filing metadata from
https://sec.gov using BeautifulSoup. One can, for example, retrieve the data for
Palantir Technologies, Inc., which has CIK 0001321655, via:

  >>> import pysec
  >>> info, num_scrapes = pysec.company_info(cik='001321655')

Similarly, Accenture plc's CIK is 1467373, so one can similarly retrieve its
filings via:

  >>> info, num_scrapes = pysec.company_info(cik='1467373')

Please be aware that there is a well-documented memory leak in the lxml parser
(which we use to get accurate table parses) due to the maintenance of a global
dictionary. Thus, it may be untenable to call this scraper from within a loop
in a single process more than a thousand or so times. See

    https://groups.google.com/g/beautifulsoup/c/ZCEsPeTQjLk

for more information.

TODO(Jack Poulson): Add support for the Standard Industrial Code and insider
transactions.
'''
import bs4
import csv
import gc
import io
import json
import pandas as pd
import requests
from bs4 import BeautifulSoup

BASE_SEC_URL = 'https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany'

# If no bytes are received by requests.get for the following number of seconds,
# then an exception will be thrown.
DEFAULT_TIMEOUT = 30.0

# Using 'lxml' instead of 'html.parser' is critical for accurately
# parsing tables. Otherwise we see spurious patterns such as
# '</tr></tr></tr>...'.
PARSER = 'lxml'


def company_info_url(cik, count=100, start=0):
    ''' Returns a URL for querying SEC documents.'''
    return '{}&CIK={}&type=&dateb=&owner=include&start={}&count={}'.format(
        BASE_SEC_URL, cik, start, count)


def company_info_soup(cik, count=100, start=0, timeout=DEFAULT_TIMEOUT):
    '''Returns a soup for company filings.'''
    url = company_info_url(cik, count, start)
    try:
        page = requests.get(url, timeout=timeout)
        return BeautifulSoup(page.content, PARSER)
    except:
        return None


def company_name(soup):
    '''Returns the company name from the document soup.'''
    nameSpan = soup.find('span', class_='companyName')
    if nameSpan is None:
        return None
    return nameSpan.contents[0].strip()


def isPhoneNumber(value):
    '''A simple test for whether a string is a phone number.'''
    return value.replace('(', '').replace(')', '').replace('-', '').replace(
        '.', '').replace(' ', '').isdigit()


def addresses(soup):
    '''Returns the mailing and business address of the business.'''
    items = {'mailing': None, 'business': None}
    for mailer in soup.find_all('div', class_="mailer"):
        numContents = len(mailer.contents)

        address = {}
        if numContents >= 6:
            if isPhoneNumber(mailer.contents[5].contents[0].strip()):
                # The last of the three items looks like a phone number.
                address['street'] = mailer.contents[1].contents[0].strip()
                address['cityStateZip'] = mailer.contents[3].contents[0].strip(
                )
                address['phoneNumber'] = mailer.contents[5].contents[0].strip()
            else:
                address['street'] = '{}, {}'.format(
                    mailer.contents[1].contents[0].strip(),
                    mailer.contents[3].contents[0].strip())
                address['cityStateZip'] = mailer.contents[5].contents[0].strip(
                )
        elif numContents >= 4:
            address['street'] = mailer.contents[1].contents[0].strip()
            address['cityStateZip'] = mailer.contents[3].contents[0].strip()
        else:
            print('Address numContents={}: {}'.format(numContents,
                                                      mailer.contents))

        label = mailer.contents[0].strip()
        if label == 'Mailing Address':
            items['mailing'] = address
        elif label == 'Business Address':
            items['business'] = address
        else:
            print('WARNING: Address label was {}'.format(label))

    return items


def filings(soup):
    docTables = soup.find_all('table', class_='tableFile2', limit=1)
    filings = []
    if len(docTables) == 0:
        return filings
    docTable = docTables[0]
    for rowIndex in range(3, len(docTable.contents), 2):
        row = docTable.contents[rowIndex]
        filing = {}
        if len(row.contents) != 11:
            print('Row contents were of length {} rather than 11: {}'.format(
                len(row.contents), row.contents))
            continue

        typeInfo = row.contents[1]
        if len(typeInfo.contents) < 1:
            print('typeInfo was too short: {}'.format(typeInfo))
        filing['type'] = typeInfo.contents[0]

        urlInfo = row.contents[3]
        if len(urlInfo.contents) < 1:
            print('urlInfo was too short: {}'.format(urlInfo))
        filing['url'] = 'https://sec.gov/{}'.format(
            urlInfo.contents[0]['href'])

        descInfo = row.contents[5]
        description = []
        for descPiece in descInfo.contents:
            if type(descPiece) is bs4.element.NavigableString:
                strippedPiece = descPiece.strip()
                if strippedPiece:
                    description.append(strippedPiece)
            elif type(descPiece) is bs4.element.Tag:
                if descPiece.content:
                    description.append(descPiece.content)
            else:
                print('skipped {}: {}'.format(type(descPiece), descPiece))
        filing['description'] = description

        dateInfo = row.contents[7]
        if len(dateInfo.contents) < 1:
            print('dateInfo was too short: {}'.format(dateInfo))
        filing['date'] = dateInfo.contents[0]

        fileAndFilmInfo = row.contents[9]
        for content in fileAndFilmInfo.contents:
            if 'href' in content:
                filing['fileNumber'] = content.contents[0]
                filing['fileNumberURL'] = 'https://sec.gov/{}'.format(
                    content['href'])
            elif type(content) is str:
                filmNumber = content.strip()
                if filmNumber:
                    filing['filmNumber'] = filmNumber

        filings.append(filing)

    return filings


def have_next_filings(soup):
    '''Returns whether there is a subsequent batch of filings to retrieve.'''
    next_filings_list = soup.find_all('input', value='Next 100')
    return len(next_filings_list) > 0


def company_info(cik, count=100, timeout=DEFAULT_TIMEOUT, verbosity=1,
    old_info=None):
    '''Returns pairing of given company's filings and the number of scrapes.'''
    url = company_info_url(cik, count)
    info = {'cik': cik}
    try:
        start = 0
        if verbosity >= 2:
            print('Extracting {} filings with start of {}'.format(
                count, start))
        page = requests.get(url, timeout=timeout)
        soup = BeautifulSoup(page.content, PARSER)
        num_scrapes = 1

        info['companyName'] = company_name(soup)
        if verbosity >= 2:
            print('Company name: {}'.format(info['companyName']))
        info['addresses'] = addresses(soup)
        if verbosity >= 2:
            print('Addresses: {}'.format(info['addresses']))

        def insert_filings(filings, new_filings):
            '''Helper function for inserting new filings into list.'''
            inserted_filing = False
            newest_date = None
            for new_filing in reversed(new_filings):
                insertion_index = 0
                if newest_date == None or new_filing['date'] > newest_date:
                    newest_date = new_filing['date']
                for index, filing in enumerate(filings):
                    if new_filing['url'] == filing['url']:
                        insertion_index = None
                        break

                if insertion_index != None:
                    inserted_filing = True
                    if verbosity >= 1:
                        print('{}: Inserting filing at index {}'.format(
                            info['companyName'], insertion_index))
                    filings.insert(insertion_index, new_filing)

            if verbosity >= 1:
                print('{}: Newest filing date {}'.format(
                    info['companyName'], newest_date))

            return inserted_filing

        if old_info:
            info['filings'] = old_info['filings']
            inserted_filings = insert_filings(info['filings'], filings(soup))
        else:
            info['filings'] = filings(soup)
            inserted_filings = True

        while inserted_filings and have_next_filings(soup):
            soup.decompose()
            del soup
            gc.collect()

            start += count
            if verbosity >= 2:
                print('Extracting {} filings with start of {}'.format(
                    count, start))
            url = company_info_url(cik, count=count, start=start)
            page = requests.get(url, timeout=timeout)
            soup = BeautifulSoup(page.content, PARSER)
            num_scrapes += 1

            if old_info:
                inserted_filings = insert_filings(
                    info['filings'], filings(soup))
            else:
                info['filings'] += filings(soup)
                inserted_filings = True

        soup.decompose()
        del soup
        gc.collect()

    except Exception as e:
        print('Caught exception during processing: {}'.format(e))

    return info, num_scrapes


def json_to_csv(json_filename, csv_filename):
    '''Converts a JSON representation of filings into a CSV.'''
    df = pd.read_json(json_filename)

    df['companyName'] = df['companyName'].apply(
        lambda value : value.lower() if value else value)
    df['addresses'] = df['addresses'].apply(lambda value : json.dumps(value))

    # We cannot lowercase the URL links, as this breaks them.
    def lowercaseFilingExceptURL(filings):
        lowerFilings = []
        for filing in filings:
            lowerFiling = {}
            if filing['date']:
              lowerFiling['date'] = filing['date'].lower()
            if filing['type']:
              lowerFiling['type'] = filing['type'].lower()
            if filing['url']:
              lowerFiling['url'] = filing['url']
            if filing['description']:
              lowerFiling['description'] = []
              for line in filing['description']:
                  lowerFiling['description'].append(line.lower().encode('ascii', 'ignore').decode())
            lowerFilings.append(lowerFiling)
        return lowerFilings

    df['filings'] = df['filings'].apply(
        lambda value : json.dumps(lowercaseFilingExceptURL(value)))

    # We must use a tab separator and avoid newlines or MySQL will truncate
    # all text inputs to 255 characters. This causes JSON parsing issues for
    # some of Google's "lobbyist_positions" fields.

    df = df.replace(r'\\r\\n', '; ', regex=True)
    df = df.replace(r'\\r', '; ', regex=True)
    df = df.replace(r'\\n', '; ', regex=True)

    df = df.replace(r'\r\n', '; ', regex=True)
    df = df.replace(r'\r', '; ', regex=True)
    df = df.replace(r'\n', '; ', regex=True)

    df = df.replace(r'\\', r'\\\\', regex=True)

    # Replace all sequences of the form '\...;' with ';' since PostgreSQL
    # refuses to parse JSON otherwise.
    df = df.replace(r'\\+;', r';', regex=True)

    df.to_csv(csv_filename,
              index=False,
              escapechar='\\',
              quotechar='"',
              doublequote=False,
              sep='\t',
              quoting=csv.QUOTE_ALL)
